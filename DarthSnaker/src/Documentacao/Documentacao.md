# **SEJA BEM VINDO A DARTH SNAKER**

![darthsnaker](https://gitlab.com/francisco170142329/ep2/raw/master/DarthSnaker/src/Documentacao/888.png)

## **Descriçao do projeto**
 O famoso jogo Snake (também conhecido como "jogo da cobrinha") é um jogo que ficou conhecido por diversas versões cuja versão inicial começou com o jogo Blockade de 1976, sendo feitas várias imitações em vídeo-games e computadores. No fim dos anos 90 foi popularizado em celulares da Nokia que vinham com o jogo já incluso.

Diferetimente do classico  essa nova versão possui algumas implementaçoes a mais, como escolha da snaker e tipos de frutas diferentes.


## **Intruções para execulta o game**
Nesse projeto a IDE [Netbeans](https://netbeans.apache.org/download/index.html) foi essencial   para o Desenvolvimento do Software.

![Netbeans](https://netbeans.org/images_www/v7/3/features/ide-basic-full.png)

**Atenção**-Fica subentedido que vc ai execulta o usando a IDE  netbeans, e de preferência o sistema operacional **Linux**.

**Priemiro passo** -> Depois de fazer o git clone do repositorio na sua maquina, click em abrir Projeto no neatbens.
~~~ 
git clone https://gitlab.com/francisco170142329/ep2.git 
~~~
![Abrir Projeto](https://gitlab.com/francisco170142329/ep2/raw/master/DarthSnaker/src/Documentacao/8.png)

**Segundo passo** -> Depois de abrir o projeto  procure a pasta **Source Packges**, Logo vc encontra as duas Camadas do game.

![Pasta src](https://gitlab.com/francisco170142329/ep2/raw/master/DarthSnaker/src/Documentacao/3.png)

**Terceiro passo** -> Abrir a camada de Apresentação,selecione a classe mainPrincipal.

![Camadas](https://gitlab.com/francisco170142329/ep2/raw/master/DarthSnaker/src/Documentacao/5.png) 

Logo depois é só execulta a class no comando run.
![](https://gitlab.com/francisco170142329/ep2/raw/master/DarthSnaker/src/Documentacao/25.png)


**Pronto, agora é só começar a jogar!!!**  
🎮🎮🎮


## O GAME ##
* Objetivo
  * Alcançar a maior pontuação possivel.
  * Sera Gerado varias frutas no campo onde a snaker se encontra, de modo aleatório e terá um tempo especifico antes que ela desaparecer a reapareça aleatoriamente em outro lugar.

* Regras
    * Se a cabeça da snaker encostar em qualquer lugar de seu corpo ela morre.
    * Se a Snaker se encontrar com a barreira limitadora ela morre.

* Especificaçoes
  * Enquanto mais tempo você passa no jogo , maior sera a velocidade da snaker.
  * O controlador do game é as setinhas do teclado.

## Pendecias ##
**Verifique-se que o java e o javac estejam instalados na sua respectiva maquina.**









